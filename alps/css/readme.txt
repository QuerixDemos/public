1. Copy hcl-main.css hcl-fonts.css and /fonts folder into the folder with hcl-common.css

2. Add line 
   @import url(./hcl-main.css);
   on top of the hcl-common.css or

Typography
Consistent communication through functional elegance
Typeface
Source Sans Pro
Our primary brand typeface for digital is purposefully designed for user interfaces. We feel it offers an excellent reading experience while not compromising in design elegance. It allows us to communicate our tone of voice with a contemporary aesthetic as well as being light weight enough technically to ensure quick load times with consistent rendering across all devices.

Weights
Using these weights from our brand font family provides visual contrast as well as consistency.
Light
To be used in headers and form labels, 20px and above. Do not use as body copy.

Regular
To be used for headers and all body copy.

Semibold
To be used in headers and copy above 16px.

Bold
For headers and highlighting key points in body copy.

Colour
HEADING GREY
#5C596D
Used for H1, H2, H3 and H4.

BODY COPY GREY
#444444
Used for bold copy.

LINK BLUE
#004FB6
Used for actions and links, anything clickable should have an element of this blue.

WHITE
#FFFFFF
White is your best friend.

Headers
We use a solid typographic hierarchy across devices to ensure guiding legibility while also drawing focus where we need it most.

Desktop
Heading zero light
70pt \ Light \ #5C596D \ 74pt Leading \ -0.2 Letter spacing \ Only to be used in hero spaces

Heading zero regular
70pt \ Regular \ #5C596D \ 74pt Leading \ -0.2 Letter spacing \ Only to be used in hero spaces

Heading zero semibold
70pt \ Semi-bold \ #5C596D \ 74pt Leading \ -0.2 Letter spacing \ Only to be used in hero spaces

Heading one light
42pt \ Light \ #5C596D \ 50pt Leading \ -0.1 Letter spacing

Heading one regular
42pt \ Regular \ #5C596D \ 50pt Leading \ -0.1 Letter spacing

Heading one semibold
42pt \ Semi-bold \ #5C596D \ 50pt Leading \ -0.1 Letter spacing

Heading two light
28pt \ Light \ #5C596D \ 36pt Leading \ -0.1 Letter spacing

Heading two regular
28pt \ Regular \ #5C596D \ 36pt Leading \ -0.1 Letter spacing

Heading two semibold
28pt \ Semi-bold \ #5C596D \ 36pt Leading \ -0.1 Letter spacing

Heading three light
20pt \ Light \ #5C596D \ 28pt Leading \ -0.1 Letter spacing

Heading three regular
20pt \ Regular \ #5C596D \ 28pt Leading \ -0.1 Letter spacing

Heading three semibold
20pt \ Semi-bold \ #5C596D \ 28pt Leading \ -0.1 Letter spacing

Heading four bold
16pt \ Bold \ #5C596D \ 24pt Leading \ -0.1 Letter spacing

Tablet
Heading zero light
68pt \ Light \ 76pt Leading \ -0.2 Letter spacing \ Only to be used in hero spaces

Heading zero regular
68pt \ Regular \ 76pt Leading \ -0.2 Letter spacing \ Only to be used in hero spaces

Heading zero semibold
68pt \ Semi-bold \ #5C596D \ 76pt Leading \ -0.2 Letter spacing \ Only to be used in hero spaces

Heading one light
36pt \ Light \ #5C596D \ 46pt Leading \ -0.1 Letter spacing

Heading one regular
36pt \ Regular \ #5C596D \ 46pt Leading \ -0.1 Letter spacing

Heading one semibold
36pt \ Semi-bold \ #5C596D \ 46pt Leading \ -0.1 Letter spacing

Heading two light
26pt \ Light \ #5C596D \ 34pt Leading \ -0.1 Letter spacing

Heading two regular
26pt \ Regular \ #5C596D \ 34pt Leading \ -0.1 Letter spacing

Heading two semibold
26pt \ Semi-bold \ #5C596D \ 34pt Leading \ -0.1 Letter spacing

Heading three light
20pt \ Light \ #5C596D \ 28pt Leading \ -0.1 Letter spacing

Heading three regular
20pt \ Regular \ #5C596D \ 28pt Leading \ -0.1 Letter spacing

Heading three semibold
20pt \ Semi-bold \ #5C596D \ 28pt Leading \ -0.1 Letter spacing

Heading four bold
16pt \ Bold \ #5C596D \ 24pt Leading \ -0.1 Letter spacing

Mobile
Heading zero light
42pt \ Light \ #5C596D \ 50pt Leading \ -0.2 Letter spacing \ Only to be used in hero spaces

Heading zero regular
42pt \ Regular \ #5C596D \ 50pt Leading \ -0.2 Letter spacing \ Only to be used in hero spaces

Heading zero semibold
42pt \ Semi-bold \ #5C596D \ 50pt Leading \ - 0.2 Letter spacing \ Only to be used in hero spaces

Heading one light
32pt \ Light \ 42pt Leading \ #5C596D \ -0.1 Letter spacing

Heading one regular
32pt \ Regular \ 42pt Leading \ #5C596D \ -0.1 Letter spacing

Heading one semibold
32pt \ Semi-bold \ 42pt Leading \ #5C596D \ -0.1 Letter spacing

Heading two light
24pt \ Light \ 34pt Leading \ #5C596D \ -0.1 Letter spacing

Heading two regular
24pt \ Regular \ 34pt Leading \ #5C596D \ -0.1 Letter spacing

Heading two semibold
24pt \ Semi-bold \ #5C596D \ 34pt Leading \ - 0.1 Letter spacing

Heading three light
20pt \ Light \ #5C596D \ 28pt Leading \ -0.1 Letter spacing

Heading three regular
20pt \ Regular \ #5C596D \ 28pt Leading \ -0.1 Letter spacing

Heading three semibold
20pt \ Semi-bold \ #5C596D \ 28pt Leading \ - 0.1 Letter spacing

Heading four bold
16pt \ Bold \ #5C596D \ 24pt Leading \ -0.1 Letter spacing

Other styles
Body copy

"The quick brown fox jumps over the lazy dog" is an English-language pangram- a phrase that contains all of the letters of the alphabet. It is commonly used for touch-typing practice. It is also used to test typewriters and computer keyboards, show font and for other applications involving all of the letters in the English alphabet. Owing to its brevity and coherence it has become widely known.

Body copy - 16pt \ Regular \ #444444 \ 24pt Leading \ 0 Letter spacing

Caption

The quick brown fox jumps over the lazy dog

Caption - 16pt \ Regular \ #444444 \ 24pt Leading \ 0 Letter spacing

Small print

"The quick brown fox jumps over the lazy dog" is an English-language pangram- a phrase that contains all of the letters of the alphabet. It is commonly used for touch-typing practice. It is also used to test typewriters and computer keyboards, show font and for other applications involving all of the letters in the English alphabet. Owing to its brevity and coherence it has become widely known.

Small print - 14pt \ Regular \ #444444 \ 22pt Leading \ 0 Letter spacing

Form label
The quick brown fox jumps over the lazy dog
Form label - 22pt \ Light \ #5C596D \ 30pt Leading \ 0 Letter spacing